package com.wen.dao;

import java.util.List;

import com.wen.dao.pojo.SaveRecord;
//	管理员对用户的操作
public interface SaveRecordDao extends BaseDao<SaveRecord> {
	List<SaveRecord> queryFromTo(String start,String end);
	int deleteToVip(String vip);
	int deleteToUser(String user);
}
