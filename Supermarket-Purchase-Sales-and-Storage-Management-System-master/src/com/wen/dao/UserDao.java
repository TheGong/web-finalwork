package com.wen.dao;

import com.wen.dao.pojo.User;
//用户信息
public interface UserDao extends BaseDao<User> {
	User login(String uid,String upassword);
	int countById(String uid);
}
